<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Profile;
use App\Models\Project;
use App\Models\Country;
use App\Models\Category;
use App\Models\Language;
use App\Models\UserLanguage;
use App\Models\Skill;
use App\Models\UserSkill;
use App\Models\Industry;
use App\Models\UserIndustry;
use App\Models\UserCharacteristic;
use App\Models\Qualification;
use App\Models\UserQualification;
use App\Models\UserClient;
use App\Models\Admin;
use App\Models\ConsultantProject;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProjectProposalSentMail;
use App\Mail\ProjectCreateMail;
use App\Models\Notification;
use Common;


use Sanitize;
use Illuminate\Support\Facades\Storage;
use URL;
use File;

class ConsultantController extends Controller
{
    // function to search consultant //
    public function search_consultant(Request $request, User $user)
    {
        // Get category list //
        $categories = Category::where('is_active', '1')->where('is_deleted', '0')->select('id','name')->get();

        // Set User query object for user search list //
        $user = $user->newQuery();

        // If search request comes from search page with additional parameter //
        if($request->ajax()){

            // applying common filter //
            $query = $user->where('user_type','Elysian')->where('is_approved','1')
            ->where('status', '1')->where('is_first','0')->whereNotNull('email_verified_at')
            ->where('is_deleted','0')

            // applying customized profile based filters //
            ->whereHas('profile', function($qry) use($request){

                //checking parameter existance //
                if(!empty($request->countries) || !empty($request->resource) || !empty($request->availability)){

                    // applying category filter //
                    $qry->where('category_id', $request->category);

                    // checking country parameter and applying filter //
                    if(!empty($request->countries)){
                        $request->countries = explode(',',$request->countries);
                        $qry->whereIn('country_id', $request->countries);
                    }

                    // checking resource type parameter and applying filter //
                    if(!empty($request->resource)){
                        $qry->where('resource_type', $request->resource);
                    }

                    // checking availability status parameter and applying filter //
                    if($request->has('availability')){
                        $qry->where('available_status', '=', $request->availability);
                    }
                }else{

                    // if no additional parameter not found apply only common category filter //
                    $qry->where('category_id', $request->category);
                }
            });

            // checking existance of expertise parameter //
            if(!empty($request->expertise)){
                $request->expertise = explode(',',$request->expertise);

                // applying user skill filter //
                $query = $query->whereHas('user_skill', function($qry) use($request){
                    $qry->whereIn('skill_id',$request->expertise);
                });
            }

            // checking languages parameter existance //
            if(!empty($request->langages)){
                $request->langages = explode(',',$request->langages);

                // applying languages filter //
                $query = $query->whereHas('languages', function($qry) use($request){
                    $qry->whereIn('language_id',$request->langages);
                });
            }

            // checking characteristics parameter existance //
            if(!empty($request->characteristics)){
                $request->characteristics = explode(',',$request->characteristics);

                // applying characteristics filter //
                $query = $query->whereHas('characteristics', function($qry) use($request){
                    $qry->whereIn('characteristic',$request->characteristics);
                });
            }

            // check and apply sorting type for sorting by name //
            if(!empty($request->shorting)){
                $query = $query->orderBy('name',$request->shorting);
            }else{
                $query = $query->orderBy('name','asc');
            }

            // paginating user search list by 6 users per page //
            $result = $query->paginate(6);
            
            // append other pagination link with result query except csrf token //
            $result->appends(request()->except(['page','_token']))->links();

            // checking if request have any previous project id //
            $project = !empty($request->project) ? $request->project : 0;
            $consult = '';
            if(!empty($project)){
                // get consultant id list by project id //
                $consult = ConsultantProject::where('project_id',$project)->pluck('consultant_id')->toArray();
            }

            // generating html view with consultant search result //
            $html = view('client.partial.search_list', compact('result', 'project', 'consult'))->render();

            // providing page count information according to the pagination result //
            $total = 0;
            $page = 0;
            if(empty($request->page)){
                if($result->hasPages()){
                    $total = (int) $result->lastPage();
                    $page = 1;
                }
            }

            return response()->json(['total' => $total, 'page' => $page, 'list' => $html]);

        // if user access search page for the first time with only category parameter //
        }elseif($request->has('category')){

            // applying common filter //
            $query = $user->where('user_type','Elysian')->where('is_approved','1')
            ->where('status', '1')->where('is_first','0')->whereNotNull('email_verified_at')
            ->where('is_deleted','0')
            ->whereHas('profile', function($qry) use($request){
                $qry->where('category_id', $request->category);

            })->inRandomOrder();

            $result = $query->paginate(6);
            $total = $query->count('id');

            // append other pagination link with result query except csrf token //
            $result->appends(request()->except(['page','_token']))->links();

            $category = $request->category;
            $skill = !empty($request->skill)? $request->skill : '';
            $project = !empty($request->project) ? $request->project : 0;
            $consult = '';

            // Getting additional filter data for filter option in the page //
            $projects = Project::where('is_assigned', '0')->where('status', '1')->where('category_id', $request->category)->where('user_id',auth()->user()->id)->select('id', 'title')->get();
            $skills = Skill::where('category_id', $request->category)->where('is_deleted','0')->where('is_active','1')->get();
            $countries = Country::where('is_active', '1')->where('is_deleted', '0')->get();
            $languages = Language::where('is_active', '1')->where('is_deleted', '0')->get();

            if($request->has('project')){
                $consult = ConsultantProject::where('project_id',$request->project)->pluck('consultant_id')->toArray();
            }

            return view('client.consultant_list', compact('result','total','category','skill','project','categories','projects','skills','countries','languages','consult'));
        }else{
            // if user access the page without any category they will redirected to home page //
            return redirect()->route('home');
        }
    }


    // function to get parent and sub category and skills accorging to the category id //
    public function get_category(Request $request)
    {
        if(!empty($request->key) && $request->ajax()){

            $categories = Category::where('name','like','%'.$request->key.'%')->whereNull('parent_id')->select('id','name')->get();

            if(count($categories) > 0){
                foreach ($categories as $key => $value) {
                    $skills = Skill::where('category_id', $value->id)->where('is_active', '1')->where('is_deleted', '0')->select('id','name')->get();
                    if(count($skills) > 0){
                        $categories[$key]->skills = $skills;
                    }else{
                        $categories[$key]->skills = '';
                    }
                }
            }else{
                $skill = Skill::where('name','like','%'.$request->key.'%')->where('is_active', '1')
                ->where('is_deleted', '0')->groupBy('category_id')->pluck('category_id');
                $categories = Category::whereIn('id',$skill)->whereNull('parent_id')->select('id','name')->get();
                if(count($categories) > 0){
                    foreach ($categories as $key => $value) {
                        $skills = Skill::where('name','like','%'.$request->key.'%')->where('category_id', $value->id)->where('is_active', '1')->where('is_deleted', '0')->select('id','name')->get();
                        if(count($skills) > 0){
                            $categories[$key]->skills = $skills;
                        }else{
                            $categories[$key]->skills = '';
                        }
                    }
                }
            }

            if(count($categories) > 0){
                return response()->json($categories);
            }else{
                return false;
            }

        }else{
            // if unauthorized accessed return to home //
            return redirect()->route('home');
        }
    }

    // function to get predefined list of projects //
    public function get_project(Request $request)
    {
        if(!empty($request->key) && $request->ajax()){
            $projects = Project::where('is_assigned', '0')->where('status', '1')->where('category_id', $request->key)->where('user_id',auth()->user()->id)->select('id', 'title')->get();
            $skills = Skill::where('category_id', $request->key)->where('is_deleted','0')->where('is_active','1')->get();
            if(count($projects) > 0 || count($skills) > 0){
                return response()->json(['projects' => $projects, 'skills' => $skills]);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    // function to send proposal to 
    public function send_proposal(Request $request)
    {
        if($request->has('project') && $request->has('consultant')){
            // checking if project is already alloted with consultant //
            $cp = ConsultantProject::where('project_id',$request->project)->where('consultant_id',$request->consultant)->first();
            if(empty($cp->id)){

                // creating new allotment record //
                $cp = new ConsultantProject();
                $cp->project_id = $request->project;
                $cp->consultant_id = $request->consultant;
                $cp->job_status = 'pending';
                $cp->save();

                $project = Project::find($request->project);

                $talent = User::find($request->consultant);

                // creating notification record//
                $notify = new Notification();
                $notify->user_id = $request->consultant;
                $notify->notification_type = 'proposal';
                $notify->save();

                $admin = Admin::find(1);

                // Admin email notification for new proposal start //
                $configSMTP = array(
                    'transport' => 'smtp',
                    'host'       => "mail.oneelysium.ch",
                    'port'       => "587",
                    'encryption' => "tls",
                    'username'   => "Connect@oneelysium.ch",
                    'password'   => "Work2021*",
                );
                $configFROM = array('address' => "Connect@oneelysium.ch", 'name' => "Elysium");
                Common::SMTPEmailWithDynamicConfig($configSMTP, $configFROM);

                $data = [
                    'client' => auth()->user(),
                    'talent' => $talent,
                    'project' => $project,
                    'receiver' => 1
                ];

                try {
                    /* send email to user */
                    Mail::to($admin->email)->send(new ProjectProposalSentMail($data));
                } catch(\Exception $e) {
                    //
                }
                // Admin email notification for new proposal end //

                // Consultant email notification for new proposal start //
                $configSMTP = array(
                    'transport' => 'smtp',
                    'host'       => "mail.oneelysium.ch",
                    'port'       => "587",
                    'encryption' => "tls",
                    'username'   => "Elysians@oneelysium.ch",
                    'password'   => "Work2021*",
                );
                $configFROM = array('address' => "Elysians@oneelysium.ch", 'name' => "Elysium");
                Common::SMTPEmailWithDynamicConfig($configSMTP, $configFROM);

                $data['receiver'] = 2;
                /* send email to user */
                Mail::to($talent->email)->send(new ProjectProposalSentMail($data));
                // Consultant email notification for new proposal end //

            }
            return $cp;
        }else{
            return false;
        }
        exit;
    }

    //function to show client proposal status //
    public function proposal_status(Project $project)
    {
        $status = ConsultantProject::where('project_id', $project->id)->where('is_approved', '1')->first();
        return view('client.proposals', compact('project', 'status'));
    }

    // function to approve a specific consultant for a specific project //
    public function approve_consultant(ConsultantProject $consult)
    {
        $project = Project::find($consult->project_id);
        $project->is_assigned = '1';
        $project->save();

        $consult->is_approved = '1';
        $consult->job_status = 'processing';
        $consult->save();

        return redirect()->route('show-payment',$project->id);
    }
}
